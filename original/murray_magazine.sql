-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 04, 2015 at 01:41 PM
-- Server version: 5.1.73-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `murray_magazine`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `articleID` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `art_date` date NOT NULL DEFAULT '0000-00-00',
  `description` text NOT NULL,
  `bio` text NOT NULL,
  `body` text NOT NULL,
  `icon` varchar(100) NOT NULL DEFAULT 'N',
  `video` varchar(100) NOT NULL,
  `published` varchar(1) NOT NULL DEFAULT 'Y',
  `social_published` enum('Y','N') NOT NULL DEFAULT 'N',
  `publish_order` int(3) NOT NULL,
  `tags` varchar(255) NOT NULL,
  PRIMARY KEY (`articleID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=710 ;

-- --------------------------------------------------------

--
-- Table structure for table `articles_custom`
--

CREATE TABLE IF NOT EXISTS `articles_custom` (
  `articleID` int(11) NOT NULL AUTO_INCREMENT,
  `rel_clubID` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `art_date` date NOT NULL DEFAULT '0000-00-00',
  `description` text NOT NULL,
  `bio` text NOT NULL,
  `body` text NOT NULL,
  `icon` varchar(100) NOT NULL DEFAULT 'N',
  `video` varchar(100) NOT NULL,
  `ordering` int(5) NOT NULL,
  `status` varchar(1) NOT NULL,
  `end_date` date NOT NULL,
  `thumb` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  PRIMARY KEY (`articleID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1416 ;

-- --------------------------------------------------------

--
-- Table structure for table `articles_edited`
--

CREATE TABLE IF NOT EXISTS `articles_edited` (
  `articleID` int(11) NOT NULL,
  `clubid` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `art_date` date NOT NULL,
  `description` text NOT NULL,
  `body` text NOT NULL,
  `icon` varchar(100) NOT NULL,
  `published` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`articleID`,`clubid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `articles_featured`
--

CREATE TABLE IF NOT EXISTS `articles_featured` (
  `featureID` int(11) NOT NULL AUTO_INCREMENT,
  `featured_date` date NOT NULL DEFAULT '0000-00-00',
  `featured1` int(5) DEFAULT NULL,
  `featured2` int(5) DEFAULT NULL,
  `featured3` int(5) DEFAULT NULL,
  `featured4` int(5) DEFAULT NULL,
  `featured5` int(5) DEFAULT NULL,
  `featured6` int(5) DEFAULT NULL,
  PRIMARY KEY (`featureID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `articles_rem`
--

CREATE TABLE IF NOT EXISTS `articles_rem` (
  `articles_remID` int(11) NOT NULL AUTO_INCREMENT,
  `relarticlesID` int(11) NOT NULL DEFAULT '0',
  `relclub_linkID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`articles_remID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `article_counter`
--

CREATE TABLE IF NOT EXISTS `article_counter` (
  `countid` int(11) NOT NULL AUTO_INCREMENT,
  `artid` int(11) NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ipaddress` varchar(150) NOT NULL DEFAULT '',
  `subdomain` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`countid`),
  KEY `artid` (`artid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1148404 ;

-- --------------------------------------------------------

--
-- Table structure for table `article_mod`
--

CREATE TABLE IF NOT EXISTS `article_mod` (
  `articleid` int(11) NOT NULL,
  `clubid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`articleid`,`clubid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_care`
--

CREATE TABLE IF NOT EXISTS `car_care` (
  `careid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `care_date` date NOT NULL DEFAULT '0000-00-00',
  `image` varchar(150) NOT NULL DEFAULT '',
  `published` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`careid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `category_rel`
--

CREATE TABLE IF NOT EXISTS `category_rel` (
  `p_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `club_colorscheme`
--

CREATE TABLE IF NOT EXISTS `club_colorscheme` (
  `colorid` int(11) NOT NULL AUTO_INCREMENT,
  `colorscheme` varchar(100) NOT NULL DEFAULT '0',
  `featured_bar` varchar(7) NOT NULL DEFAULT '',
  `featured_title` varchar(7) NOT NULL DEFAULT '',
  `menu_bar` varchar(7) NOT NULL DEFAULT '',
  `left_bar` varchar(7) NOT NULL,
  `left_title` varchar(7) NOT NULL,
  `left_over` varchar(7) NOT NULL,
  `menu_text` varchar(7) NOT NULL,
  `background` varchar(7) NOT NULL,
  `clubid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`colorid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `club_cover`
--

CREATE TABLE IF NOT EXISTS `club_cover` (
  `coverid` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `relclub_linkID` int(11) NOT NULL,
  `cover_date` date NOT NULL,
  PRIMARY KEY (`coverid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `club_frontpage`
--

CREATE TABLE IF NOT EXISTS `club_frontpage` (
  `frontpageid` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` text NOT NULL,
  `clubid` varchar(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`frontpageid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `club_links`
--

CREATE TABLE IF NOT EXISTS `club_links` (
  `club_linkID` bigint(32) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `home_link` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(255) NOT NULL DEFAULT '',
  `club_name` varchar(255) NOT NULL DEFAULT '',
  `website` varchar(255) NOT NULL DEFAULT '',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `ads` int(2) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `email_count` int(11) NOT NULL DEFAULT '0',
  `location` varchar(200) NOT NULL DEFAULT 'hamilton',
  `industry` varchar(100) NOT NULL DEFAULT 'automotive',
  `analytics` text NOT NULL,
  `web_form` text NOT NULL,
  `blog` varchar(255) NOT NULL,
  `user_key` varchar(255) DEFAULT NULL,
  `frontpageid` int(5) NOT NULL,
  `logo` text NOT NULL,
  `color` varchar(7) NOT NULL,
  `custom` varchar(1) NOT NULL DEFAULT 'N',
  `right_image` text NOT NULL,
  `header_text` text NOT NULL,
  `footer_limage` varchar(255) NOT NULL,
  `footer_rimage` varchar(255) NOT NULL,
  `templateid` int(11) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `prograde` varchar(255) NOT NULL,
  `shakeology` varchar(255) NOT NULL,
  `contact` text NOT NULL,
  `athleticgreens` varchar(50) NOT NULL,
  `UNIT` enum('US','Metric','','') NOT NULL DEFAULT 'US',
  `recipes` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`club_linkID`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=426 ;

-- --------------------------------------------------------

--
-- Table structure for table `club_mod`
--

CREATE TABLE IF NOT EXISTS `club_mod` (
  `club_modID` int(11) NOT NULL AUTO_INCREMENT,
  `body_mod` varchar(100) NOT NULL DEFAULT '',
  `family_mod` varchar(100) NOT NULL DEFAULT '',
  `diet_mod` varchar(100) NOT NULL DEFAULT '',
  `health_mod` varchar(100) NOT NULL DEFAULT '',
  `spirit_mod` varchar(100) NOT NULL DEFAULT '',
  `time_mod` varchar(100) NOT NULL DEFAULT '',
  `tool_mod` varchar(100) NOT NULL DEFAULT '',
  `relclub_linkID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`club_modID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `club_settings`
--

CREATE TABLE IF NOT EXISTS `club_settings` (
  `relclub_linkID` int(11) NOT NULL DEFAULT '0',
  `menu_bar` varchar(7) NOT NULL DEFAULT '',
  `featured_bar` varchar(7) NOT NULL DEFAULT '',
  `sub_bar` varchar(7) NOT NULL DEFAULT '',
  `featured_title` varchar(7) NOT NULL DEFAULT '',
  `sub_title` varchar(7) NOT NULL DEFAULT '',
  PRIMARY KEY (`relclub_linkID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `club_templatecolors`
--

CREATE TABLE IF NOT EXISTS `club_templatecolors` (
  `templateid` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `main_nav` varchar(20) NOT NULL,
  `main_nav_hover` varchar(20) NOT NULL,
  `directory` varchar(200) NOT NULL,
  `cover_line` varchar(20) NOT NULL,
  `news_area` varchar(20) NOT NULL,
  `download_box` varchar(20) NOT NULL,
  `common_box` varchar(20) NOT NULL,
  `inner_common_box` varchar(20) NOT NULL,
  `weather_box` varchar(20) NOT NULL,
  `foot_nav` varchar(20) NOT NULL,
  `foot_nav_text` varchar(20) NOT NULL,
  `copyright` varchar(20) NOT NULL,
  `follow_text` varchar(20) NOT NULL,
  PRIMARY KEY (`templateid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `club_templates`
--

CREATE TABLE IF NOT EXISTS `club_templates` (
  `templateid` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) NOT NULL,
  `background` varchar(7) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`templateid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `fb_pages`
--

CREATE TABLE IF NOT EXISTS `fb_pages` (
  `user_id` int(11) NOT NULL,
  `page_id` bigint(20) NOT NULL,
  `page_auth` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `posting` varchar(1) NOT NULL DEFAULT 'Y',
  `art_posting` varchar(1) NOT NULL DEFAULT 'Y',
  `filter` varchar(1) NOT NULL DEFAULT 'N',
  `dairy_filter` varchar(1) NOT NULL DEFAULT 'N',
  `is_assign` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fitness_tips`
--

CREATE TABLE IF NOT EXISTS `fitness_tips` (
  `tipid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `tip_date` date NOT NULL DEFAULT '0000-00-00',
  `image` varchar(150) NOT NULL DEFAULT '',
  `published` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`tipid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

-- --------------------------------------------------------

--
-- Table structure for table `linkedin_info`
--

CREATE TABLE IF NOT EXISTS `linkedin_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE IF NOT EXISTS `manager` (
  `managerid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `manager_date` date NOT NULL DEFAULT '0000-00-00',
  `rel_clubid` int(11) NOT NULL DEFAULT '0',
  `video` varchar(150) NOT NULL,
  PRIMARY KEY (`managerid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=258 ;

-- --------------------------------------------------------

--
-- Table structure for table `marketing_tips`
--

CREATE TABLE IF NOT EXISTS `marketing_tips` (
  `tipid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `tip_date` date NOT NULL DEFAULT '0000-00-00',
  `image` varchar(150) NOT NULL DEFAULT '',
  `published` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`tipid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_clicks`
--

CREATE TABLE IF NOT EXISTS `phpdig_clicks` (
  `c_num` mediumint(9) NOT NULL,
  `c_url` varchar(255) NOT NULL DEFAULT '',
  `c_val` varchar(255) NOT NULL DEFAULT '',
  `c_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_engine`
--

CREATE TABLE IF NOT EXISTS `phpdig_engine` (
  `spider_id` mediumint(9) NOT NULL DEFAULT '0',
  `key_id` mediumint(9) NOT NULL DEFAULT '0',
  `weight` smallint(4) NOT NULL DEFAULT '0',
  KEY `key_id` (`key_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_excludes`
--

CREATE TABLE IF NOT EXISTS `phpdig_excludes` (
  `ex_id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `ex_site_id` mediumint(9) NOT NULL,
  `ex_path` text NOT NULL,
  PRIMARY KEY (`ex_id`),
  KEY `ex_site_id` (`ex_site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_includes`
--

CREATE TABLE IF NOT EXISTS `phpdig_includes` (
  `in_id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `in_site_id` mediumint(9) NOT NULL,
  `in_path` text NOT NULL,
  PRIMARY KEY (`in_id`),
  KEY `in_site_id` (`in_site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_keywords`
--

CREATE TABLE IF NOT EXISTS `phpdig_keywords` (
  `key_id` int(9) NOT NULL AUTO_INCREMENT,
  `twoletters` char(2) NOT NULL,
  `keyword` varchar(64) NOT NULL,
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `keyword` (`keyword`),
  KEY `twoletters` (`twoletters`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23441 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_logs`
--

CREATE TABLE IF NOT EXISTS `phpdig_logs` (
  `l_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `l_includes` varchar(255) NOT NULL DEFAULT '',
  `l_excludes` varchar(127) DEFAULT NULL,
  `l_num` mediumint(9) DEFAULT NULL,
  `l_mode` char(1) DEFAULT NULL,
  `l_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `l_time` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`l_id`),
  KEY `l_includes` (`l_includes`),
  KEY `l_excludes` (`l_excludes`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_sites`
--

CREATE TABLE IF NOT EXISTS `phpdig_sites` (
  `site_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `site_url` varchar(127) NOT NULL,
  `upddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `port` smallint(6) DEFAULT NULL,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `stopped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_site_page`
--

CREATE TABLE IF NOT EXISTS `phpdig_site_page` (
  `site_id` int(4) NOT NULL,
  `days` int(4) NOT NULL DEFAULT '0',
  `links` int(4) NOT NULL DEFAULT '5',
  `depth` int(4) NOT NULL DEFAULT '5',
  PRIMARY KEY (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_spider`
--

CREATE TABLE IF NOT EXISTS `phpdig_spider` (
  `spider_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `file` varchar(127) NOT NULL,
  `first_words` mediumtext NOT NULL,
  `upddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `md5` varchar(50) DEFAULT NULL,
  `site_id` mediumint(9) NOT NULL DEFAULT '0',
  `path` varchar(127) NOT NULL,
  `num_words` int(11) NOT NULL DEFAULT '1',
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filesize` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`spider_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1070 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpdig_tempspider`
--

CREATE TABLE IF NOT EXISTS `phpdig_tempspider` (
  `file` text NOT NULL,
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `level` tinyint(6) NOT NULL DEFAULT '0',
  `path` text NOT NULL,
  `site_id` mediumint(9) NOT NULL DEFAULT '0',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `upddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `error` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1430 ;

-- --------------------------------------------------------

--
-- Table structure for table `pingfm_info`
--

CREATE TABLE IF NOT EXISTS `pingfm_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `id` varchar(11) NOT NULL,
  `total_votes` int(11) NOT NULL DEFAULT '0',
  `total_value` int(11) NOT NULL DEFAULT '0',
  `used_ips` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `ingredients_and_quantity` text NOT NULL,
  `nutrition_facts` text NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `instruction` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` text NOT NULL,
  `p_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE IF NOT EXISTS `tips` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tip` text NOT NULL,
  `image` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1005 ;

-- --------------------------------------------------------

--
-- Table structure for table `tips_used`
--

CREATE TABLE IF NOT EXISTS `tips_used` (
  `tipid` int(11) NOT NULL,
  `page_id` bigint(20) NOT NULL,
  `tip_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tipid`,`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `twtinfo`
--

CREATE TABLE IF NOT EXISTS `twtinfo` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `twitter_token` varchar(255) DEFAULT NULL,
  `twitter_verifier` varchar(255) DEFAULT NULL,
  `connection` text NOT NULL,
  `userid` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=94 ;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fbid` bigint(64) NOT NULL DEFAULT '0',
  `fbsession` varchar(255) NOT NULL DEFAULT '',
  `fbtoken` varchar(255) NOT NULL DEFAULT '',
  `userid` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `videoID` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `video_date` date NOT NULL,
  `published` varchar(1) NOT NULL,
  `clubID` int(11) NOT NULL,
  PRIMARY KEY (`videoID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `videoID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `longdesc` text NOT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`videoID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Table structure for table `weather`
--

CREATE TABLE IF NOT EXISTS `weather` (
  `weatherid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` text NOT NULL,
  PRIMARY KEY (`weatherid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
