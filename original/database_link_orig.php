<?php
//
// SourceForge: Breaking Down the Barriers to Open Source Development
// Copyright 1999-2000 (c) The SourceForge Crew
// http://sourceforge.net
//
// $Id: database.php,v 1.6 2000/04/11 14:17:13 cvs Exp $
//
// /etc/local.inc includes the machine specific database connect info

$sys_dbhost   = 'localhost';
$sys_dbuser   = 'murray_php';
$sys_dbpasswd = 'murray2009';
//$sys_dbname='djensen_magazine';

function db_connect()
{
    global $sys_dbhost, $sys_dbuser, $sys_dbpasswd, $conn;
    $conn = mysql_connect($sys_dbhost, $sys_dbuser, $sys_dbpasswd);
    if (!$conn) {
        echo mysql_error();
    }

    //mysql_close($conn);
    return $conn;
}

function db_checkAndReconnect()
{
    global $conn;
    if (!mysql_ping($conn)) {
        mysql_close($conn);
        db_connect();
        if (!$conn) {
            die('Could not reconnect to mysql after timeout: ' . mysql_error());
        }
    }
}

function db_close()
{
    global $conn;
    mysql_close($conn);
}

function db_query($qstring, $dbname)
{
    //global $sys_dbname;
    db_checkAndReconnect();

    // This should be replaced
    return @mysql($dbname, $qstring);
    // by this
    /*
    if (!mysql_select_db($dbname)) {
        echo 'Could not select database';
        return false;
    }
    return @mysql_query($qstring);
    */
}

function db_numrows($qhandle)
{
    // return only if qhandle exists, otherwise 0
    db_checkAndReconnect();
    if ($qhandle) {
        return @mysql_numrows($qhandle);
    } else {
        return 0;
    }
}

function db_result($qhandle, $row, $field)
{
    db_checkAndReconnect();

    return @mysql_result($qhandle, $row, $field);
}

function db_numfields($lhandle)
{
    db_checkAndReconnect();

    return @mysql_numfields($lhandle);
}

function db_fieldname($lhandle, $fnumber)
{
    db_checkAndReconnect();

    return @mysql_fieldname($lhandle, $fnumber);
}

function db_affected_rows($qhandle)
{
    db_checkAndReconnect();

    return @mysql_affected_rows();
}

function db_fetch_array($qhandle)
{
    db_checkAndReconnect();

    return @mysql_fetch_array($qhandle);
}

function db_fetch_row($qhandle)
{
    db_checkAndReconnect();

    return @mysql_fetch_row($qhandle);
}

function db_insertid($qhandle)
{
    db_checkAndReconnect();

    return @mysql_insert_id($qhandle);
}

function db_error()
{
    return "\n\n<P><B>" . @mysql_error() . "</B><P>\n\n";
}

//connect to the db
//I usually call from pre.php
db_connect();

?>