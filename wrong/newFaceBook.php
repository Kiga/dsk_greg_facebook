<?php
use Facebook\FacebookRequest;
use Facebook\FacebookSession;

class newFaceBook
{
    private $session;
    private $appsecret_proof;

    public function __construct($token, $appSecret)
    {
        $this->acceptSessionByToken($token);
        $this->acceptProof($token, $appSecret);
    }

    public function acceptSessionByToken($token)
    {
//        $this->setSession(new FacebookSession($token));
        $this->setSession(FacebookSession::newAppSession(838122206240938, 'fba331f69129a057185c89c05e3c97d3'));
    }

    /**
     * @param mixed $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }

    public function makeRequest($page, $message, $imagePath = null)
    {
        $postParams = $this->getPostParams($message, $imagePath);
        $request    = new FacebookRequest($this->session, 'POST', $page, $postParams);

        return $request->execute()->getGraphObject();
    }

    public function makeGetRequest($page, $message, $imagePath = null)
    {
        $postParams = $this->getPostParams($message, $imagePath);
        $request    = new FacebookRequest($this->session, 'GET', $page, $postParams);

        return $request->execute()->getGraphObject();
    }

    private function imageMime($filePath)
    {
        $fh = fopen($filePath, 'rb');
        if ($fh) {
            $bytes6 = fread($fh, 6);
            fclose($fh);
            if ($bytes6 === false) {
                return false;
            }
            if (substr($bytes6, 0, 3) == "\xff\xd8\xff") {
                return 'image/jpeg';
            }
            if ($bytes6 == "\x89PNG\x0d\x0a") {
                return 'image/png';
            }
            if ($bytes6 == "GIF87a" || $bytes6 == "GIF89a") {
                return 'image/gif';
            }

            return 'application/octet-stream';
        }

        return false;
    }

    /**
     * @param $message
     * @param $imagePath
     *
     * @return array
     */
    private function getPostParams($message, $imagePath)
    {
        $postParams = array(
            'message'         => $message,
            'appsecret_proof' => $this->appsecret_proof
        );
        if (null !== $imagePath) {
            $realPath             = @realpath($imagePath);
            $postParams['source'] = new CURLFile($realPath, $this->imageMime($realPath));

            return $postParams;
        }

        return $postParams;
    }

    /**
     * @param $token
     * @param $appSecret
     */
    private function acceptProof($token, $appSecret)
    {
        $this->appsecret_proof = hash_hmac('sha256', $token, $appSecret);
    }
}
